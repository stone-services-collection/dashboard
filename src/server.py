import os
import os.path
from os.path import expanduser
from system.scripts import amber
from system.scripts import sec
from flask import Flask, render_template, request, redirect, url_for, session

# Create App Folder
HOMEDIR = expanduser("~")
APP_FOLDER = HOMEDIR + "/Latch/S-Dashboard"
try: os.mkdir(HOMEDIR + "/Latch")
except: pass
try: os.mkdir(HOMEDIR + "/Latch/S-Dashboard")
except: pass
try: os.mkdir(HOMEDIR + "/Latch/S-Dashboard/data")
except: pass
if (not os.path.isfile(HOMEDIR + "/Latch/S-Dashboard/data/notifications")):
        with open(HOMEDIR + "/Latch/S-Dashboard/data/notifications", 'w') as x:
            pass

# Set Hard Values
with open('./data/registered.json', 'r') as data_file:
    a = data_file.read()


app = Flask(__name__)
uptime = os.popen("uptime | awk '{ print $1 }'").read()

# Security Control
def security_control():
    pass

# Get home.html
@app.route("/")
def home():
    return render_template('home/home.html')

# GET security.html
@app.route("/security/")
def security():
    return render_template('security/security.html')

# GET login.html
@app.route("/login")
def login():
    return render_template("login/login.html")

# GET drivers
@app.route("/drivers")
def drivers():
    return driver_list

# GET server
@app.route("/server")
def server():
    return {
        "uptime": "test",
        "loadavg": "load",
        "error": "error"
    }

# GET/POST Notifications
@app.route("/notification", methods=["GET", "POST"])
def read_notification():
    if (request.method == "GET"):
        with open(HOMEDIR + "/Latch/S-Dashboard/data/notifications", 'r') as doc:
            return doc.readlines()
    elif (request.method == "POST"): 
        print(request.form['action'])
        return "hi kent"

# GET/POST AI
@app.route('/ai', methods=['GET', 'POST'])
def ai():
    x = request.form['data']
    y = amber.amber_control(x)
    return y

@app.route("/cli", methods=["GET", "POST"])
def cli():
    x = request.form['data']
    y = sec.cli(x)
    print(y + "===============")
    return y

# === External Incoming Route === #
@app.route("/mobile", methods=["GET", "POST"])
def mobile():
    device = request.form['device']
    command = request.form['command']
    print(device, command)
    return "ello kent"
# ============== #

if __name__ == '__main__':
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'    
    app.run(debug=True, host="0.0.0.0", port=1111)
