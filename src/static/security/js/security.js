(function() {
  function one() {
    let bot_1 = document.getElementsByClassName("security__items")[0]
    $('.security__items').on('click', function() {
      let a = $(this)
      if ($(this).attr("data-status") == 'off') {
        $(this).find('img').hide()
        $(this).find('.go').show()
        $(this).attr("data-status", 'on')
      } else {
        $(this).find('img').hide()
        $(this).find('.no').hide().show()
        $(this).attr("data-status", 'off')
      }
    })
  }

  one()

  $(".change__pane").on("click", function() {
    console.log($(this).attr('data-pane'))
    if ($(this).attr('data-pane') == 'container1') {
      $(".container").show()
      $(".container2").hide()
    } else {
      $(".container").hide()
      $(".container2").show()
    }
  })
})()

let input;
let count = 0;
let n = "tc"
let term = document.getElementsByClassName('term')[0]

function two() {
  input = document.createElement('input')
  let d = document.createElement('div')
  let label = document.createElement("label")
  const p1Text = document.createTextNode("$");

  label.appendChild(p1Text)
  label.setAttribute('class', 'term_label')
  input.setAttribute('class', `term__input tc${count}`)
  input.setAttribute('type', "text")
  input.setAttribute('spellcheck', "false")
  input.setAttribute("onkeypress", "enterKeyPressed(event)")

  d.setAttribute("class", 'tt')
  d.appendChild(label)
  d.appendChild(input)
  term.appendChild(d)
}
two()

function enterKeyPressed(event) {
  if (event.keyCode == 13) {
    let out = document.createElement("div")
    let aa;

    $.post("/cli", {data:input.value}, function(result) {
      let p1Text = document.createTextNode(result);

      out.appendChild(p1Text)
      out.setAttribute("class", "hh")
      out.append(p1Text)
      term.appendChild(out)
      aa = document.getElementsByClassName(`tc${count}`)[0]
      aa.setAttribute("disabled", "disabled")

      count += 1
      two()
      $(`.tc${count}`).focus()
      $('.term').scrollTop($('.term')[0].scrollHeight);
    })
  }
}



let robot1 = document.getElementsByClassName('robot-1')[0]
let robot2 = document.getElementsByClassName('robot-2')[0]
let robot3 = document.getElementsByClassName('robot-3')[0]
let robotSelection = document.getElementsByClassName('robot--selection')[0]

robot1.addEventListener('click', function() {
  robotSelection.textContent = "M1"
  console.log("m clicked")
})
robot2.addEventListener('click', function() {
  robotSelection.textContent = "M2"
})

robot3.addEventListener('click', function() {
  robotSelection.textContent = "M3"
})

