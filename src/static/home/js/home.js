let max = 1
let amount = 0;

// GEAR SELECT
function gearSelect() {
  $('.gear__select').click(function() {
    console.log("z")
    $('.entity__list').hide()
    let a = $(this).attr('data-for')
    $(`.entity__list[data-name=${a}]`).show()
    $('.log__img').css('background-image', `url(${$(this).attr('data-img')})`)
    
    if (a == "entity-server") {
      $.get("/server", function(data, status){
        $('.uptime').text(data['uptime'])
        $('.loadavg').text(data['loadavg'])
        $('.error').text(data['error'])
      })
    }
  })

  $('.entity__btn').click(function() {
    var s = ($(this).text())
    let img = $(this).attr("data-img")
    $('.driver__list').hide()
    console.log(s)
    $(`.driver__list[data-name="${s}"]`).show()
    console.log($(this).attr("data-img"))
    $('.entity__img').css('background-image', `url(${img})`)
  })
}

// AI INTERACTION
function popupConsole() {
  $('#console-drag header').mousedown(function() {
    $('.draggable').draggable();
    $('.draggable').draggable('enable');
  }).mouseup(function() {$('.draggable').draggable('disable');})

  $('.btn-console').on('click', function() {
    $(".popup-console").toggle()
  })

  $('.ai__input').on('keypress',function(e) {
    var d = new Date();
    var timestamp = d.getHours() + ":" +d.getMinutes()
    if(e.which == 13) {
      let userInput = $(this).val()
      $(this).val("")
      $('.ai__chat').append(`<label>[${timestamp}] ${userInput}</label> <br>`)
      $('.ai__chat').scrollTop($('.ai__chat')[0].scrollHeight);

      $.post("/ai", {data:userInput}, function(result) {
        $('.ai__chat').append(`<label>[${timestamp}] ${result}</label> <br>`)
      })
    }
  });
}


// Close APP

function closeAPP() {
  frame = document.getElementsByClassName('app')[0]
  frameClose = document.getElementsByClassName('frameClose')[0]
  frameClose.addEventListener("click", function() {

    frame.style.display = "none"
  })
}

closeAPP()
// Box 
function box() {
  $('.box-nav__img').on("click", function() {
    targetView = $(this).attr('data-for')
    $('.box-view').hide()
    if (targetView == "log") {
      $('.box-logs').show()
    } else if (targetView == "wallpaper") {
      $('.box-wallpapers').show()
    } else if (targetView == "weather") {
      $('.box-weather').show()
    } else if (targetView == "devices") {
      $('.box-devices').show()
    }
  })
}

box()

// Popup Access
function popupAccess() {
  let spinner = document.getElementsByClassName('pupup__spinner')[0]
  let bb = document.getElementsByClassName('box-icon')[1]
  let popup = document.getElementsByClassName("popup-access")[0]
  let popupBtnCancel = document.getElementsByClassName("popup__cancel")[0]
  let tool = document.getElementsByClassName('btn-access')[0]
  let spinning = false;
  
  let accessDrag = $("#access-drag")
  $('header').mousedown(function() {
    $('.draggable').draggable();
    $('.draggable').draggable('enable');
  }).mouseup(function() {$('.draggable').draggable('disable');})

  popupBtnCancel.addEventListener('click', function() {
    popup.style.display = "none"
    bb.style.borderColor = '#262626'
  })
  
  async function spinnerControl() {
    let num = 1;
    if (spinning == false) {
      spinning = true
      setInterval(() => {
        num += 1
        spinner.style.transform = `rotate(${num}deg)`
      }, 5);
    }
  }

  tool.addEventListener('click', function() {
    spinnerControl()
    popup.style.display = 'initial'
  })
}

// DRIVERS
$('.entity__driver').click(function() {
  let version = $(this).text()
  $('.driver__version').text(version)
})
$('.drone__mode').click(function() {
  $('.drone__mode').css("opacity", ".5")
  $(this).css("opacity", "1")
})
$('.entity__btn').click(function() {
  let name = $(this).text()
  $('.driver__name').text(name)
})

function changeView(e) {
  let a = document.getElementsByClassName('view')
  for (let index = 0; index < a.length; index++) {
    a[index].style.display = 'none'
  }
  console.log(e)
  document.getElementsByClassName(`view__${e.getAttribute('data-for')}`)[0].style.display = 'initial'
}

// WALLPAPER
function wallpapers() {
  $(".wallpaper-thumb").on("click", function() {
    let thumbUrl = $(this).attr('data-for')
    // $('body').css('background-image', `url(/static/home/img/wallpaper/${thumbUrl}.jpg)`)
    $('body').css('background-image', `url(/static/home/img/wallpaper/${thumbUrl}.svg)`)
  })
}

// NOTIFICATION
function notification() {
  $.get("/notification", function(data, status){
    data.forEach(element => {
      $('.notifications-list').append(`<p>${element}</p>`)
      console.log(element)
    });
  })
}

wallpapers()

popupConsole()
popupAccess()
notification() 
gearSelect()