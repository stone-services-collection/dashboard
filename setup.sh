# Debian/Ubuntu - Dashbarod Setup Script
# will automaticly start the Dashboard service after installation completes

setup() {
	echo "Installing for Debian/Ubuntu"

	echo "Installing python"
	apt install python2 &> /dev/null
	apt install python3 &> /dev/null
	
	echo "Installing pip"
	apt install python3-pip &> /dev/null
	
	echo "Installing Flask"
	pip3 install flask &> /dev/null

	echo "Installing Tornado"
	pip3 install tornado &> /dev/null
	pip install terminado &> /dev/null

	echo "READY!"
	
	echo "=== Starting Dashboard Server =="
	
	cd src && python3 server.py
}

setup
