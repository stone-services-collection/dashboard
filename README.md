# Dashboard

Dashboard is a python flask web server used for managing the security of the home is running inside along with various tweaks to the server's hardware. 
 

# Structure / Environment 
Dashboard is targed at Ubuntu Server, but can be modified for any operating system.

## Code Base
- **C** - low-level system programming
- **Python** - Flask code file and modules
- **Bash** - Build script used for compiling the application and installing dependencies
- **HTML/CSS** - Web UI structure and design

## Hierarchy
- **/src** - Location for source code 
- **/src/static** - Location for CSS, JS, and IMG files
- **/src/templates/** - Location for HTML files
- **/src/system/** - Local system utilites 
